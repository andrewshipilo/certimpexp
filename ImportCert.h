#pragma once
#include <string>
#include <iostream>
#include <windows.h>
#include <cryptuiapi.h>
#include <codecvt>


#define F_INFO BY_HANDLE_FILE_INFORMATION 

class ImportCert
{
private:
	long m_fileSize;
	HANDLE m_fileHandle;
	BYTE* m_buffer;
	HCERTSTORE m_certStore = NULL;
	HCERTSTORE m_myCertStore = NULL;
	CRYPT_DATA_BLOB m_cryptBlob;
	PCCERT_CONTEXT m_context = NULL;

public:
	explicit ImportCert() {}

	~ImportCert() 
	{
		CertCloseStore(m_certStore, 0);
		CertCloseStore(m_myCertStore, 0);
	}
	
	void Import(const std::string &filename,
				const std::string &password)
	{
		FileRead(filename);
		CreateCertStore(password);
		AddContextToStore();

		printf("Done.");
	}

	
	
private:
	void FileRead(const std::string & filename)
	{
		auto fileName = filename.c_str();
		printf("Reading certificate file: %s\n", fileName);

		F_INFO fileInfo;
		m_fileHandle = CreateFile(fileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		if (m_fileHandle == INVALID_HANDLE_VALUE)
		{
			printf("Can't open file %s\n", fileName);
			return;
		}

		GetFileInformationByHandle(m_fileHandle, &fileInfo);
		m_fileSize = fileInfo.nFileIndexLow;

		// File buffer
		m_buffer = new BYTE[m_fileSize];

		unsigned long bytesRead;
		ReadFile(m_fileHandle, m_buffer, m_fileSize, &bytesRead, NULL);
	}

	void CreateCertStore(const std::string & password)
	{
		printf("Creating certificate store\n");

		m_cryptBlob.cbData = m_fileSize;
		m_cryptBlob.pbData = m_buffer;

		if (!PFXIsPFXBlob(&m_cryptBlob))
		{
			printf("Problem with PFXIsPFXBlob\n");
			return;
		}

		std::wstring stemp = std::wstring(password.begin(), password.end());
		LPCWSTR pass = stemp.c_str();
		m_certStore = PFXImportCertStore(&m_cryptBlob, pass, CRYPT_USER_KEYSET);
		if (m_certStore == NULL)
		{
			printf("Problem with PFXImportCertStore\n");
			return;
		}
	}

	void AddContextToStore()
	{
		m_myCertStore = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, L"MY");

		while (NULL != (m_context = CertEnumCertificatesInStore(m_certStore, m_context)))
		{
			char certName[256];
			CertGetNameString(m_context, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, certName, 128);
			printf("Certificate name: %s\n", certName);
			CertAddCertificateContextToStore(m_myCertStore, m_context, CERT_STORE_ADD_ALWAYS, NULL);
		}
	}

	std::wstring s2ws(const std::string& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;
		return r;
	}

};