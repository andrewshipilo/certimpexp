#include "InputParser.h"
#include "ImportCert.h"
#include "ExportCert.h"
#include <iostream>
#include <stdio.h>

int main(int argc, char* argv[])
{
	InputParser input(argc, argv);

	const std::string &filename = input.getArg("-f");
	const std::string &password = input.getArg("-p");

	if (input.hasArg("-i")) // Import mode
	{
		printf("Importing certificate\n");
		ImportCert().Import(filename, password);
	}

	if (input.hasArg("-e"))
	{
		printf("Exporting certificate\n");
		ExportCert().Export(filename, password, input);
	}
	// -i  -f C:\Certificates\lab4\fd.p12 -p phrase
	// -e  -f C:\Certificates\lab4\test.p12 -p phrase -is ALOHASUB
	// -e  -f C:\Certificates\lab4\test.p12 -p phrase -s lab14
	int x;
	std::cin >> x;
	return 0;
}