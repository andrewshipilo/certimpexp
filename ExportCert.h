#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <iostream>
#include <fstream>
#include "InputParser.h"

#define BEAUTY_ENCODING (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
void MyHandleError(TCHAR* s);

enum class SearchType
{
	NONE = 0,
	ISSUER,
	SUBJECT,
	TIME
};

class ExportCert
{
private:
	HCRYPTPROV m_cryptProv = NULL;
	HCERTSTORE m_certStore = NULL;
	PCCERT_CONTEXT m_context = NULL;
	CRYPT_DATA_BLOB cryptBlob;
	HCRYPTKEY m_key;
	HCERTSTORE m_store = NULL;
	CRYPT_KEY_PROV_INFO m_keyInfo;
	char m_certName[256];
	WCHAR m_keyContainerName[256];
public:
	explicit ExportCert() {}

	~ExportCert()
	{
		CertFreeCertificateContext(m_context);
		CryptDestroyKey(m_key);
		CryptReleaseContext(m_cryptProv, 0);

	}

	void Export(const std::string &filename,
				const std::string &password,
				const InputParser &input)
	{
		std::ofstream out(filename, std::ios::binary);
		if (!out)
		{
			printf("Can't write to file %s\n", filename.c_str());
			return;
		}

		if (!ChooseProviderType())
			return;

		printf("Opening certificate store...");
		m_certStore = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, L"MY");

		if (m_certStore != NULL)
		{
			printf("\tOK\n");
		}
		else
		{
			printf("\tERROR\n");
			printf("Can't open MY certificate store\n");
			return;
		}

		switch (CheckSearchType(input))
		{
		case SearchType::NONE:
			printf("Please provide program with search type (-is, -s, -t)\n");
			return;
		case SearchType::ISSUER:
			if (!ExportByIssuer(input))
				return;
			break;
		case SearchType::SUBJECT:
			if (!ExportBySubject(input))
				return;
			break;
		case SearchType::TIME:
			if (!ExportByTime(input))
				return;
			break;
		}

		if (!OpenStoreAndGenKey())
			return;

		ZeroMemory(&cryptBlob, sizeof(cryptBlob));
		std::wstring stemp = std::wstring(password.begin(), password.end());
		LPCWSTR pass = stemp.c_str();

		if (!PFXExportCertStoreEx(m_store, &cryptBlob, pass, NULL, EXPORT_PRIVATE_KEYS) || cryptBlob.cbData == 0)
		{
			printf("Can't export a certificate from the certificate store\n");
			return;
		}
		cryptBlob.pbData = (PBYTE)_alloca(cryptBlob.cbData);
		if (!PFXExportCertStoreEx(m_store, &cryptBlob, pass, NULL, EXPORT_PRIVATE_KEYS) || cryptBlob.cbData == 0)
		{
			printf("Can't export a certificate from the certificate store\n");
			return;
		}

		out.write((char*)cryptBlob.pbData, cryptBlob.cbData);
		printf("Done.\n");
	}

private:
	bool OpenStoreAndGenKey()
	{
		if (!CryptGenKey(m_cryptProv, AT_KEYEXCHANGE, CRYPT_EXPORTABLE, &m_key)) 
		{
			printf("Can't generate a key pair\n");
			return false;
		}

		
		m_store = CertOpenStore(CERT_STORE_PROV_MEMORY, 0, 0, CERT_STORE_CREATE_NEW_FLAG, 0);
		if (m_store == NULL)
		{
			printf("Can't open this store\n");
			return false;
		}

		printf("Adding certificate context to store...");
		if (CertAddCertificateContextToStore(m_store, m_context, CERT_STORE_ADD_ALWAYS, NULL))
		{
			printf("\tOK\n");
		}
		else 
		{
			printf("\tERROR\n");
			return false;
		}

		
		ZeroMemory(&m_keyInfo, sizeof(m_keyInfo));
		m_keyInfo.pwszContainerName = m_keyContainerName;
		m_keyInfo.pwszProvName = (LPWSTR)MS_DEF_PROV;
		m_keyInfo.dwProvType = PROV_RSA_FULL;
		m_keyInfo.dwFlags = CRYPT_MACHINE_KEYSET;
		m_keyInfo.dwKeySpec = AT_KEYEXCHANGE;
		m_context = NULL;
		return true;
	}

	bool ExportByIssuer(const InputParser &input)
	{
		std::wstring stemp = std::wstring(input.getArg("-is").begin(), input.getArg("-is").end());
		LPCWSTR issuer = stemp.c_str();

		m_context = NULL;
		if (m_context = CertFindCertificateInStore(m_certStore, BEAUTY_ENCODING, 0, CERT_FIND_ISSUER_STR, issuer, NULL))
		{
			printf("The desired certificate was found\n");
		}
		else
		{
			printf("Could not find the desired certificate\n");
			return false;
		}
		CertGetNameString(m_context, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, m_certName, 128);
		printf("Certificate name %s\n", m_certName);
		return true;
	}

	bool ExportBySubject(const InputParser &input)
	{
		std::wstring stemp = std::wstring(input.getArg("-s").begin(), input.getArg("-s").end());
		LPCWSTR subject = stemp.c_str();

		m_context = NULL;
		if (m_context = CertFindCertificateInStore(m_certStore, BEAUTY_ENCODING, 0, CERT_FIND_SUBJECT_STR, subject, NULL))
		{
			printf("The desired certificate was found\n");
		}
		else
		{
			printf("Could not find the desired certificate\n");
			return false;
		}
		CertGetNameString(m_context, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, m_certName, 128);
		printf("Certificate name %s\n", m_certName);
		return true;
	}

	bool ExportByTime(const InputParser &input)
	{
		HCERTSTORE thisStore;
		thisStore = m_certStore;
		unsigned short month, day, year;
		printf("Please enter month, day and year\n");
		std::cin >> month >> day >> year;
		SYSTEMTIME stUTC;
		PCCERT_CONTEXT prev = NULL;
		int flag = 0;
		while (1) {
			if (m_context = CertFindCertificateInStore(thisStore, BEAUTY_ENCODING, 0, CERT_FIND_ANY, NULL, prev)) {
				prev = m_context;
				FileTimeToSystemTime(&m_context->pCertInfo->NotAfter, &stUTC);
				m_context = NULL;

				if (stUTC.wYear <= year && !(stUTC.wMonth > month && stUTC.wYear == year) && (!(stUTC.wDay > day && stUTC.wMonth == month && stUTC.wYear == year))) {
					std::cout << "was found" << std::endl;
					CertGetNameString(prev, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, m_certName, 128);
					printf("Certificate name: %s\n", m_certName);
					flag = 1;
					break;
				}
			}
			if (flag == 1)
				break;
		}
	}

	SearchType CheckSearchType(const InputParser &input)
	{
		if (input.hasArg("-is"))
		{
			return SearchType::ISSUER;
		}
		else if (input.hasArg("-s"))
		{
			return SearchType::SUBJECT;
		}
		else if (input.hasArg("-t"))
		{
			return SearchType::TIME;
		}
		else
		{
			return SearchType::NONE;
		}
	}

	bool ChooseProviderType()
	{
		LPTSTR pszName;


		printf(TEXT("Listing Available Provider Types.\n"));
		printf(TEXT("Provider type    Provider Type Name\n"));
		printf(TEXT("_____________    ")TEXT("_____________________________________\n"));


		DWORD dwIndex = 0;
		DWORD dwType;
		DWORD cbName;
		LPTSTR pbProvName;
		DWORD cbProvName;

		while (CryptEnumProviderTypes(dwIndex, NULL, 0, &dwType, NULL, &cbName))
		{
			if (!(pszName = (LPTSTR)LocalAlloc(LMEM_ZEROINIT, cbName)))
			{
				MyHandleError((TCHAR*)("ERROR - LocalAlloc failed!"));
			}

			if (CryptEnumProviderTypes(dwIndex++, NULL, NULL, &dwType, pszName, &cbName))
			{
				printf(TEXT("     %4.0d        %s\n"), dwType, pszName);
			}
			else
			{
				MyHandleError((TCHAR*)("ERROR - CryptEnumProviders"));
			}
			LocalFree(pszName);
		}

		printf(TEXT("\n\nListing Available Providers.\n"));
		printf(TEXT("Provider type    Provider Name\n"));
		printf(TEXT("_____________    ")TEXT("_____________________________________\n"));


		dwIndex = 0;
		while (CryptEnumProviders(dwIndex, NULL, 0, &dwType, NULL, &cbName))
		{

			if (!(pszName = (LPTSTR)LocalAlloc(LMEM_ZEROINIT, cbName)))
				MyHandleError((TCHAR*)("ERROR - LocalAlloc failed!"));

			if (CryptEnumProviders(dwIndex++, NULL, 0, &dwType, pszName, &cbName))
			{
				printf(TEXT("     %4.0d        %s\n"), dwType, pszName);
			}
			else
			{
				MyHandleError((TCHAR*)("ERROR - CryptEnumProviders"));
			}
			LocalFree(pszName);
		}

		int selectCryptProv = 0;
		printf("Please, enter number of your desired cryptoprovider\n");
		std::cin >> selectCryptProv;


		if (!(CryptGetDefaultProvider(PROV_RSA_FULL, NULL, CRYPT_MACHINE_DEFAULT, NULL, &cbProvName)))
			MyHandleError((TCHAR*)("Error getting the length of the default provider name."));

		if (!(pbProvName = (LPTSTR)LocalAlloc(LMEM_ZEROINIT, cbProvName)))
			MyHandleError((TCHAR*)("Error during memory allocation for provider name."));

		LocalFree(pbProvName);

		wcscpy_s(m_keyContainerName, L"Test Container Name");

		if (!CryptAcquireContext(&m_cryptProv, (LPCSTR)m_keyContainerName, NULL, selectCryptProv, CRYPT_NEWKEYSET | CRYPT_MACHINE_KEYSET))
		{
			if (GetLastError() == NTE_EXISTS) 
			{
				if (!CryptAcquireContext(&m_cryptProv, (LPCSTR)m_keyContainerName, MS_DEF_PROV, PROV_RSA_FULL, CRYPT_MACHINE_KEYSET))
				{
					std::cerr << "Can't get a crypto provider" << std::endl;
					return false;
				}
			}
		}
		return true;
	}

};