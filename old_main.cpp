#define _CRT_SECURE_NO_WARNINGS
//#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <stdio.h>
#include <wincrypt.h>
#include <iostream>
#include <cryptuiapi.h>
#include <string.h>
#include <tchar.h>
#include <fstream>
#pragma comment(lib, "crypt32.lib")
#define MY_ENCODING_TYPE (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
typedef unsigned long ull;

void MyHandleError(TCHAR* s);

int xmain(int argc, char* argv[]) {

	//usage :: ��� ������� argv[1] = i, ��� ������� argv[1] = e
	//argv[2] = file(import(.p12)|export(.pfx))
	//argv[3] = password
	//argv[4] = ��� ������������ ����� ��� ������� s - ����� �� subject i - ����� �� issuer t - ����� �� �������
	//argv[5] = ���� argv[4] == "s" || "i" �� ������� ������ �������� �����������(subject name)/�������� ��������
	



	PBYTE pbBuffer = NULL;
	HANDLE hFile;
	PCCERT_CONTEXT pCertContext = NULL;
	HCERTSTORE hCertStore, hMyCertStore, hRootCertStore;
	hCertStore = NULL;
	CRYPT_DATA_BLOB cryptBlob;
	WCHAR pw[256], subjName[256], issuer[256], day[6], month[6], year[6];
	int n;
	long fileSize;
	LPCSTR lpszCertSubject, lpszCertSubject1;
	DWORD dwType;
	DWORD cbName;
	DWORD dwIndex = 0;
	BYTE* ptr;
	ALG_ID aiAlgid;
	DWORD dwBits;
	DWORD dwNameLen;
	CHAR szName[100];
	BYTE pbData[1024];
	DWORD cbData = 1024;
	DWORD dwIncrement = sizeof(DWORD);
	DWORD dwFlags = CRYPT_FIRST;
	DWORD dwParam = PP_CLIENT_HWND;
	CHAR* pszAlgType = NULL;
	BOOL fMore = TRUE;
	LPTSTR pbProvName;
	DWORD cbProvName;
	PCCERT_CONTEXT prev = NULL;

	if (strcmp(argv[1], "i") == 0) {
		std::cout << "Reading Certificate file: %s " << argv[2] << std::endl;
		BY_HANDLE_FILE_INFORMATION fileInfo;
		hFile = CreateFile(argv[2], GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
		if (hFile == INVALID_HANDLE_VALUE) {
			std::cout << "Couldn't open file %s " << argv[2] << std::endl;
			return 1;
		}
		GetFileInformationByHandle(hFile, &fileInfo);
		fileSize = fileInfo.nFileSizeLow;

		//make buffer for cert data
		if (!(pbBuffer = (PBYTE)malloc(fileSize))) {
			std::cout << "Error: malloc failed (%lu)" << fileSize << std::endl;
			return 1;
		}

		ull bytesRead;
		ReadFile(hFile, pbBuffer, fileSize, &bytesRead, NULL);
		//create pfx blob
		cryptBlob.cbData = fileSize;
		cryptBlob.pbData = pbBuffer;
		printf("\nCheck\n");
		if (FALSE == PFXIsPFXBlob(&cryptBlob)) {
			std::cout << "Error: PFXIsPFXBlob failed" << std::endl;
			return 1;
		}

		MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, argv[3], -1, pw, sizeof(pw) / sizeof(WCHAR)); // UTF-8 -> UTF-16
		mbstowcs(pw, argv[3], strlen(argv[3]));
		hCertStore = PFXImportCertStore(&cryptBlob, (LPCWSTR)pw, CRYPT_EXPORTABLE);
		if (hCertStore == NULL)
			std::cout << "PFXImportCertStore failed\n" << std::endl;

		pCertContext = NULL;
		pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext);


		if (!(hMyCertStore = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, L"MY"))) { //CERT_SYSTEM_STORE_CURRENT_USER // CERT_SYSTEM_STORE_LOCAL_MACHINE
			printf("Error: CertOpenStore(MY) failed\n");
			return 1;
		}


		if (!(hRootCertStore = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, L"ROOT"))) {
			printf("Error: CertOpenStore(ROOT) failed\n");
			return 1;
		}

		HCERTSTORE thisStore;

		char pszNameString[256];
		CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
		printf("Certificate name: %s\n", pszNameString);

		if (strstr(pszNameString, "ca"))
			thisStore = hRootCertStore;
		else
			thisStore = hMyCertStore;
		CertAddCertificateContextToStore(thisStore, pCertContext, CERT_STORE_ADD_ALWAYS, NULL);
		while (NULL != (pCertContext = CertEnumCertificatesInStore(hCertStore, pCertContext))) {
			CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
			printf("Certificate name: %s\n", pszNameString);
			if (strstr(pszNameString, "ca"))
				thisStore = hRootCertStore;
			else
				thisStore = hMyCertStore;
			CertAddCertificateContextToStore(thisStore, pCertContext, CERT_STORE_ADD_ALWAYS, NULL);
		}

		// close stores
		CertCloseStore(hMyCertStore, 0);
		CertCloseStore(hRootCertStore, 0);
		CertCloseStore(hCertStore, 0);

		std::cout << "Successfully imported certificates." << std::endl;
		std::cout << "Success" << std::endl;

		// SecureZeroMoney for free memory ??????????????????????????????????????????????:
	}

	else if (strcmp(argv[1], "e") == 0) {


		std::ofstream out(argv[2], std::ios::binary);


		HCRYPTPROV hCryptProv = NULL;
		LPTSTR pszName;
		_tprintf(TEXT("Listing Available Provider Types.\n"));
		_tprintf(TEXT("Provider type    Provider Type Name\n"));
		_tprintf(TEXT("_____________    ")TEXT("_____________________________________\n"));
			   		 	  

		dwIndex = 0;
		while (CryptEnumProviderTypes(dwIndex, NULL, 0, &dwType, NULL, &cbName)) {
			//-----------------------------------------------------------
			// cbName is the length of the name of the next provider 
			// type.

			// Allocate memory in a buffer to retrieve that name.
			if (!(pszName = (LPTSTR)LocalAlloc(LMEM_ZEROINIT, cbName)))
				MyHandleError((TCHAR*)("ERROR - LocalAlloc failed!"));

			//-----------------------------------------------------------
			// Get the provider type name.
			if (CryptEnumProviderTypes(dwIndex++, NULL, NULL, &dwType, pszName, &cbName))
				_tprintf(TEXT("     %4.0d        %s\n"), dwType, pszName);
			else
				MyHandleError((TCHAR*)("ERROR - CryptEnumProviders"));
			LocalFree(pszName);
		}

		//--------------------------------------------------------------
		// Print header lines for providers.
		_tprintf(TEXT("\n\nListing Available Providers.\n"));
		_tprintf(TEXT("Provider type    Provider Name\n"));
		_tprintf(TEXT("_____________    ")TEXT("_____________________________________\n"));

		//---------------------------------------------------------------
		// Loop through enumerating providers.
		dwIndex = 0;
		while (CryptEnumProviders(dwIndex, NULL, 0, &dwType, NULL, &cbName)) {
			//-----------------------------------------------------------
			// cbName is the length of the name of the next provider.
			// Allocate memory in a buffer to retrieve that name.
			if (!(pszName = (LPTSTR)LocalAlloc(LMEM_ZEROINIT, cbName)))
				MyHandleError((TCHAR*)("ERROR - LocalAlloc failed!"));
			//-----------------------------------------------------------
			// Get the provider name.
			if (CryptEnumProviders(dwIndex++, NULL, 0, &dwType, pszName, &cbName))
				_tprintf(TEXT("     %4.0d        %s\n"), dwType, pszName);
			else
				MyHandleError((TCHAR*)("ERROR - CryptEnumProviders"));
			LocalFree(pszName);
		} // End while loop.

		//---------------------------------------------------------------
		// Get the name of the default CSP specified for the 
		// PROV_RSA_FULL type for the computer.
		int selectCryptProv = 0;
		printf("Input your select cryptoprovider, which we gonna select\n");
		std::cin >> selectCryptProv;
		//---------------------------------------------------------------
		// Get the length of the RSA_FULL default provider name.
		if (!(CryptGetDefaultProvider(PROV_RSA_FULL, NULL, CRYPT_MACHINE_DEFAULT, NULL, &cbProvName)))
			MyHandleError((TCHAR*)("Error getting the length of the default provider name."));

		//---------------------------------------------------------------
		// Allocate local memory for the name of the default provider.
		if (!(pbProvName = (LPTSTR)LocalAlloc(LMEM_ZEROINIT, cbProvName)))
			MyHandleError((TCHAR*)("Error during memory allocation for provider name."));

		//---------------------------------------------------------------
		// Get the name of the default PROV_RSA_FULL provider.
		/*if (CryptGetDefaultProvider(selectCryptProv, NULL, CRYPT_MACHINE_DEFAULT, pbProvName, &cbProvName))
			_tprintf(TEXT("\nThe default provider name is \"%s\"\n"), pbProvName);
		else
			MyHandleError((TCHAR*)("Getting the provider name failed."));
			*/
		LocalFree(pbProvName);

		//-----------------------------------------------------
		//  Acquire a cryptographic context.
		WCHAR pszKeyContainerName[256] = L"Test Container Name";

		if (!CryptAcquireContext(&hCryptProv, (LPCSTR)pszKeyContainerName, NULL, selectCryptProv, CRYPT_NEWKEYSET | CRYPT_MACHINE_KEYSET))
			if (GetLastError() == NTE_EXISTS)
				if (!CryptAcquireContext(&hCryptProv, (LPCSTR)pszKeyContainerName, MS_DEF_PROV, PROV_RSA_FULL, CRYPT_MACHINE_KEYSET)) {
					std::cerr << "Can't get a crypto provider" << std::endl;
					return 1;
				}




		MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, argv[3], -1, pw, sizeof(pw) / sizeof(WCHAR)); // UTF-8 -> UTF-16
		mbstowcs(pw, argv[3], strlen(argv[3]));
			   		 	  


		if (hCertStore = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, L"MY"))
		{
			std::cout << "Opened the system store." << std::endl;
		}
		else {
			std::cout << "Could not open the system store." << std::endl;
			return 1;
		}

		char pszNameString[256];
		if (strcmp(argv[4], "s") == 0) {
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, argv[5], -1, subjName, sizeof(subjName) / sizeof(WCHAR)); // UTF-8 -> UTF-16
			mbstowcs(subjName, argv[5], strlen(argv[5]));
			pCertContext = NULL;
			if (pCertContext = CertFindCertificateInStore(hCertStore, MY_ENCODING_TYPE, 0, CERT_FIND_SUBJECT_STR, subjName, NULL))
				std::cout << "The desired certificate was found. \n" << std::endl;
			else
				std::cout << "Could not find the desired certificate." << std::endl;

			CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
			printf("Certificate name: %s\n", pszNameString);
		}
		else if (strcmp(argv[4], "i") == 0) {
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, argv[5], -1, issuer, sizeof(issuer) / sizeof(WCHAR)); // UTF-8 -> UTF-16 
			mbstowcs(issuer, argv[5], strlen(argv[5]));
			pCertContext = NULL;
			if (pCertContext = CertFindCertificateInStore(hCertStore, MY_ENCODING_TYPE, 0, CERT_FIND_ISSUER_STR, issuer, NULL))
				std::cout << "The desired certificate was found. \n" << std::endl;
			else
				std::cout << "Could not find the desired certificate." << std::endl;
			CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
		}
		else if (strcmp(argv[4], "t") == 0) {
			HCERTSTORE thisStore2;
			thisStore2 = hCertStore;
			unsigned short month, day, year;
			std::cout << "Input your month,day, year not after certificate validate" << std::endl;
			std::cin >> month >> day >> year;
			SYSTEMTIME stUTC;
			int flag = 0;
			while (1) {
				if (pCertContext = CertFindCertificateInStore(thisStore2, MY_ENCODING_TYPE, 0, CERT_FIND_ANY, NULL, prev)) {
					prev = pCertContext;
					FileTimeToSystemTime(&pCertContext->pCertInfo->NotAfter, &stUTC);
					pCertContext = NULL;

					if (stUTC.wYear <= year && !(stUTC.wMonth > month && stUTC.wYear == year) && (!(stUTC.wDay > day && stUTC.wMonth == month && stUTC.wYear == year))) {
						std::cout << "was found" << std::endl;
						CertGetNameString(prev, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
						printf("Certificate name: %s\n", pszNameString);
						flag = 1;
						break;
					}
				}
				if (flag == 1)
					break;
			}
		}




		HCRYPTKEY hKey;
		if (!CryptGenKey(hCryptProv, AT_KEYEXCHANGE, CRYPT_EXPORTABLE, &hKey)) {
			CryptReleaseContext(hCryptProv, 0);
			std::cerr << "Can't generate a key pair" << std::endl;
			return 1;
		}

		HCERTSTORE thisStore;
		thisStore = CertOpenStore(CERT_STORE_PROV_MEMORY, 0, 0, CERT_STORE_CREATE_NEW_FLAG, 0);
		if (thisStore == NULL) {
			std::cout << " Error thisstore" << std::endl;
			return 1;
		}
		if (prev != NULL)
			pCertContext = prev;

		if (CertAddCertificateContextToStore(thisStore, pCertContext, CERT_STORE_ADD_ALWAYS, NULL))
			std::cout << "good" << std::endl;
		else {
			CryptDestroyKey(hKey);
			CertFreeCertificateContext(pCertContext);
			CryptReleaseContext(hCryptProv, 0);
			std::cout << "bad" << std::endl;
			return 1;
		}



		CRYPT_KEY_PROV_INFO ckp;
		ZeroMemory(&ckp, sizeof(ckp));
		ckp.pwszContainerName = pszKeyContainerName;
		ckp.pwszProvName = (LPWSTR)MS_DEF_PROV;
		ckp.dwProvType = PROV_RSA_FULL;
		ckp.dwFlags = CRYPT_MACHINE_KEYSET;
		ckp.dwKeySpec = AT_KEYEXCHANGE;
		pCertContext = NULL;



		ZeroMemory(&cryptBlob, sizeof(cryptBlob));


		if (!PFXExportCertStoreEx(thisStore, &cryptBlob, pw, NULL, EXPORT_PRIVATE_KEYS) || cryptBlob.cbData == 0) {
			CertFreeCertificateContext(pCertContext);
			pCertContext = NULL;
			CryptDestroyKey(hKey);
			CryptReleaseContext(hCryptProv, 0);
			std::cerr << "Can't export a certificate from the certificate store" << std::endl;
			return 1;
		}

		cryptBlob.pbData = (PBYTE)_alloca(cryptBlob.cbData);

		if (!PFXExportCertStoreEx(thisStore, &cryptBlob, pw, NULL, EXPORT_PRIVATE_KEYS) || cryptBlob.cbData == 0) {
			CertFreeCertificateContext(pCertContext);
			pCertContext = NULL;
			CryptDestroyKey(hKey);
			CryptReleaseContext(hCryptProv, 0);
			std::cerr << "Can't export a certificate from the certificate store" << std::endl;
			return 1;
		}

		if (!out) {
			CertFreeCertificateContext(pCertContext);
			pCertContext = NULL;
			CryptDestroyKey(hKey);
			CryptReleaseContext(hCryptProv, 0);
			std::cerr << "Can't write a pfx blob" << std::endl;
			return 1;
		}
		out.write((char*)cryptBlob.pbData, cryptBlob.cbData);

		CertFreeCertificateContext(pCertContext);
		pCertContext = NULL;
		CryptDestroyKey(hKey);
		CryptReleaseContext(hCryptProv, 0);
	}
	getchar();
	getchar();
	return 0;
}

void MyHandleError(TCHAR *s) {
	_tprintf(TEXT("An error occurred in running the program.\n"));
	_tprintf(TEXT("%s\n"), s);
	_tprintf(TEXT("Error number %x\n."), GetLastError());
	_tprintf(TEXT("Program terminating.\n"));
	exit(1);
}